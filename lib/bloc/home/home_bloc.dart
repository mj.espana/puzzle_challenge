import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image/image.dart' as imglib;
import 'package:puzzle/model/piece_image.dart';
import 'package:puzzle/model/position.dart';
import 'package:puzzle/utils/constants.dart';
import 'package:puzzle/utils/utils.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeState()) {
    on<SelectImageOptionEvent>(
      (event, emit) => _mapSelectImageOptionEventToState(event, emit),
    );
    on<SelectSizeOptionEvent>(
      (event, emit) => _mapSelectSizeOptionEventToState(event, emit),
    );
    on<StartPuzzleEvent>(
      (event, emit) => _mapStartPuzzleEventToState(event, emit),
    );
  }

  void _mapSelectImageOptionEventToState(
      SelectImageOptionEvent event, Emitter<HomeState> emit) {
    emit(state.copyWith(
      status: HomeStateEnum.initial,
      optionImageSelected: event.value,
    ));
  }

  void _mapSelectSizeOptionEventToState(
      SelectSizeOptionEvent event, Emitter<HomeState> emit) {
    emit(state.copyWith(
      status: HomeStateEnum.initial,
      optionSizeSelected: event.value,
    ));
  }

  void _mapStartPuzzleEventToState(
      StartPuzzleEvent event, Emitter<HomeState> emit) async {
    emit(state.copyWith(status: HomeStateEnum.loading));

    final Position emptyPosition =
        getPositionEmptyPiece(state.optionSizeSelected);
    final puzzle = await _getImageSplit(
        state.optionImageSelected, state.optionSizeSelected, emptyPosition);

    emit(state.copyWith(
      status: HomeStateEnum.success,
      puzzleImages: puzzle,
      emptyPosition: emptyPosition,
    ));
  }

  Future<List<PieceImage>> _getImageSplit(
      int imageSelected, int size, Position emptyPosition) async {
    List<Image> imagesList =
        await _splitImage(_getPathImage(imageSelected), size);
    List<Widget> imagesListWithEmpty = [];
    imagesListWithEmpty.addAll(imagesList.sublist(0, imagesList.length - 1));
    List<PieceImage> images = [];
    for (var i = 0; i < imagesListWithEmpty.length; i++) {
      images.add(PieceImage(imagesListWithEmpty[i], i + 1));
    }
    final int index = emptyPosition.x * size + emptyPosition.y;
    images.shuffle();
    images.insert(index, PieceImage(Container(), 0));
    return images;
  }

  String _getPathImage(int imageSelected) {
    if (imageSelected == 1) {
      return image1;
    } else if (imageSelected == 2) {
      return image2;
    } else {
      return '';
    }
  }

  Future<List<Image>> _splitImage(String pathImage, int numSplit) async {
    final byteData = await rootBundle.load(pathImage);

    // convert image to image from image package
    imglib.Image? image = imglib.decodeImage(byteData.buffer.asUint8List());

    int x = 0, y = 0;
    int width = (image!.width / numSplit).round();
    int height = (image.height / numSplit).round();

    // split image to parts
    List<imglib.Image> parts = [];
    for (int i = 0; i < numSplit; i++) {
      for (int j = 0; j < numSplit; j++) {
        parts.add(imglib.copyCrop(image, x, y, width, height));
        x += width;
      }
      x = 0;
      y += height;
    }

    List<Image> output = [];
    for (var img in parts) {
      output.add(Image.memory(Uint8List.fromList(imglib.encodeJpg(img))));
    }

    return output;
  }
}
