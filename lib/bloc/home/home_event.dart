part of 'home_bloc.dart';

class HomeEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class SelectImageOptionEvent extends HomeEvent {
  final int? value;

  SelectImageOptionEvent({required this.value});

  @override
  List<Object?> get props => [value];
}

class SelectSizeOptionEvent extends HomeEvent {
  final int? value;

  SelectSizeOptionEvent({required this.value});

  @override
  List<Object?> get props => [value];
}

class StartPuzzleEvent extends HomeEvent {
  @override
  List<Object?> get props => [];
}
