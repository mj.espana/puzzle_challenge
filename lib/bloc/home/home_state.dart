part of 'home_bloc.dart';

enum HomeStateEnum { initial, loading, success, failure }

extension HomeStatusX on HomeStateEnum {
  bool get isInitial => this == HomeStateEnum.initial;
  bool get isLoading => this == HomeStateEnum.loading;
  bool get isSuccess => this == HomeStateEnum.success;
  bool get isFailure => this == HomeStateEnum.failure;
}

class HomeState extends Equatable {
  HomeState({
    this.status = HomeStateEnum.initial,
    int? optionImageSelected,
    int? optionSizeSelected,
    List<PieceImage>? puzzleImages,
    Position? emptyPosition,
  })  : optionImageSelected = optionImageSelected ?? 0,
        optionSizeSelected = optionSizeSelected ?? 0,
        puzzleImages = puzzleImages ?? List.empty(),
        emptyPosition = emptyPosition ?? Position(0, 0);

  final int optionImageSelected;
  final int optionSizeSelected;
  final List<PieceImage> puzzleImages;
  final Position emptyPosition;
  final HomeStateEnum status;

  @override
  List<Object?> get props => [
        status,
        optionImageSelected,
        optionSizeSelected,
        puzzleImages,
      ];

  HomeState copyWith({
    int? optionImageSelected,
    int? optionSizeSelected,
    List<PieceImage>? puzzleImages,
    Position? emptyPosition,
    HomeStateEnum? status,
  }) {
    return HomeState(
      optionImageSelected: optionImageSelected ?? this.optionImageSelected,
      optionSizeSelected: optionSizeSelected ?? this.optionSizeSelected,
      puzzleImages: puzzleImages ?? this.puzzleImages,
      emptyPosition: emptyPosition ?? this.emptyPosition,
      status: status ?? this.status,
    );
  }
}
