import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:puzzle/model/piece.dart';
import 'package:puzzle/model/piece_image.dart';
import 'package:puzzle/model/position.dart';
import 'package:puzzle/utils/utils.dart';

part 'puzzle_event.dart';
part 'puzzle_state.dart';

class PuzzleBloc extends Bloc<PuzzleEvent, PuzzleState> {
  PuzzleBloc() : super(PuzzleState()) {
    on<LoadDataEvent>(_mapLoadDataEventToState);
    on<MovePieceEvent>(_mapMovePieceEventToState);
    on<ShuffleEvent>(_mapShuffleEventToState);
    on<MovePieceVoiceEvent>(_mapMovePieceVoiceEventToState);
    on<OnFinishEvent>(_mapOnFinishEventToState);
  }

  void _mapLoadDataEventToState(
      LoadDataEvent event, Emitter<PuzzleState> emit) async {
    emit(state.copyWith(status: PuzzleStateEnum.loading));

    final List<Piece> _puzzle = _buildPuzzle(
      event.emptyPosition,
      event.piecesImage,
      event.sizeSelected,
    );

    emit(state.copyWith(
      status: PuzzleStateEnum.build,
      emptyPosition: event.emptyPosition,
      piecesImage: event.piecesImage,
      sizeSelected: event.sizeSelected,
      puzzle: _puzzle,
    ));
  }

  void _mapMovePieceEventToState(
      MovePieceEvent event, Emitter<PuzzleState> emit) {
    final Piece emptyPiece = state.puzzle[
        state.emptyPosition.x * state.sizeSelected + state.emptyPosition.y];

    state.puzzle[state.emptyPosition.x * state.sizeSelected +
            state.emptyPosition.y] =
        state.puzzle[event.position.x * state.sizeSelected + event.position.y];

    state.puzzle[event.position.x * state.sizeSelected + event.position.y] =
        emptyPiece;

    final _puzzle =
        _rebuildPuzzle(state.sizeSelected, state.puzzle, event.position);

    emit(state.copyWith(
      status: PuzzleStateEnum.rebuild,
      emptyPosition: event.position,
      puzzle: _puzzle,
    ));
  }

  void _mapShuffleEventToState(ShuffleEvent event, Emitter<PuzzleState> emit) {
    emit(state.copyWith(status: PuzzleStateEnum.loading));

    final Position emptyPosition = getPositionEmptyPiece(event.sizeSelected);
    List<PieceImage> shufflePieces = event.piecesImage;
    shufflePieces
        .remove(shufflePieces.firstWhere((piece) => piece.number == 0));
    final int index = emptyPosition.x * event.sizeSelected + emptyPosition.y;
    shufflePieces.shuffle();
    shufflePieces.insert(index, PieceImage(Container(), 0));

    final List<Piece> _puzzle = _buildPuzzle(
      emptyPosition,
      shufflePieces,
      event.sizeSelected,
    );

    emit(state.copyWith(
      status: PuzzleStateEnum.shuffle,
      emptyPosition: emptyPosition,
      piecesImage: shufflePieces,
      sizeSelected: event.sizeSelected,
      puzzle: _puzzle,
    ));
  }

  void _mapMovePieceVoiceEventToState(
      MovePieceVoiceEvent event, Emitter<PuzzleState> emit) {
    final word = event.word.toLowerCase();
    final numberPiece = getNumberPiece(word, state.sizeSelected);
    if (numberPiece != -1) {
      final Piece pieceToMove = state.puzzle
          .where((piece) => piece.child.number == numberPiece)
          .first;

      if (isDraggable(
        pieceToMove.currentPosition.x,
        pieceToMove.currentPosition.y,
        state.emptyPosition,
        state.sizeSelected,
      )) {
        final Piece emptyPiece = state.puzzle[
            state.emptyPosition.x * state.sizeSelected + state.emptyPosition.y];

        state.puzzle[state.emptyPosition.x * state.sizeSelected +
                state.emptyPosition.y] =
            state.puzzle[pieceToMove.currentPosition.x * state.sizeSelected +
                pieceToMove.currentPosition.y];

        state.puzzle[pieceToMove.currentPosition.x * state.sizeSelected +
            pieceToMove.currentPosition.y] = emptyPiece;

        final _puzzle = _rebuildPuzzle(
            state.sizeSelected, state.puzzle, pieceToMove.currentPosition);

        emit(state.copyWith(
          status: PuzzleStateEnum.rebuildWithVoice,
          emptyPosition: pieceToMove.currentPosition,
          puzzle: _puzzle,
          numPieceMovedByVoice: pieceToMove.child.number,
        ));
      } else {
        emit(state.copyWith(
          status: PuzzleStateEnum.rebuild,
        ));
      }
    }
  }

  void _mapOnFinishEventToState(
      OnFinishEvent event, Emitter<PuzzleState> emit) {
    emit(state.copyWith(status: PuzzleStateEnum.loading));

    bool isFinished = true;
    state.puzzle.asMap().forEach((index, piece) {
      if (piece.child.number != 0 && piece.child.number != (index + 1)) {
        isFinished = false;
      }
    });

    isFinished
        ? emit(state.copyWith(status: PuzzleStateEnum.finish))
        : emit(state.copyWith(status: PuzzleStateEnum.noFinish));
  }

  List<Piece> _buildPuzzle(
    Position emptyPosition,
    List<PieceImage> piecesImage,
    int sizeSelected,
  ) {
    List<Piece> allPieces = [];
    List.generate(
      sizeSelected,
      (i) => List.generate(
        sizeSelected,
        (j) => allPieces.add(
          Piece(
            child: piecesImage[i * sizeSelected + j],
            isEmpty: isEmptyPositionPiece(i, j, emptyPosition),
            currentPosition: Position(i, j),
            emptyPosition: emptyPosition,
            sizePuzzle: sizeSelected,
          ),
        ),
      ),
    );
    return allPieces;
  }

  List<Piece> _rebuildPuzzle(
    int sizeSelected,
    List<Piece> puzzle,
    Position newEmptyPosition,
  ) {
    List<Piece> allPieces = [];
    List.generate(
      sizeSelected,
      (i) => List.generate(
        sizeSelected,
        (j) => allPieces.add(
          Piece(
            child: puzzle[i * sizeSelected + j].child,
            isEmpty: isEmptyPositionPiece(i, j, newEmptyPosition),
            currentPosition: Position(i, j),
            emptyPosition: newEmptyPosition,
            sizePuzzle: sizeSelected,
          ),
        ),
      ),
    );
    return allPieces;
  }
}
