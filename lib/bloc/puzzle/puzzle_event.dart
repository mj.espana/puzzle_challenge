part of 'puzzle_bloc.dart';

class PuzzleEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class LoadDataEvent extends PuzzleEvent {
  final int sizeSelected;
  final List<PieceImage> piecesImage;
  final Position emptyPosition;

  LoadDataEvent({
    required this.sizeSelected,
    required this.piecesImage,
    required this.emptyPosition,
  });

  @override
  List<Object?> get props => [sizeSelected, piecesImage, emptyPosition];
}

class MovePieceEvent extends PuzzleEvent {
  final Position position;

  MovePieceEvent({required this.position});

  @override
  List<Object?> get props => [position];
}

class ShuffleEvent extends PuzzleEvent {
  final int sizeSelected;
  final List<PieceImage> piecesImage;

  ShuffleEvent({
    required this.sizeSelected,
    required this.piecesImage,
  });

  @override
  List<Object?> get props => [sizeSelected, piecesImage];
}

class MovePieceVoiceEvent extends PuzzleEvent {
  final String word;

  MovePieceVoiceEvent({required this.word});

  @override
  List<Object?> get props => [word];
}

class OnFinishEvent extends PuzzleEvent {
  @override
  List<Object?> get props => [];
}
