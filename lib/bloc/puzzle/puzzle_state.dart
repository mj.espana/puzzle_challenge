part of 'puzzle_bloc.dart';

enum PuzzleStateEnum {
  initial,
  loading,
  build,
  rebuild,
  rebuildWithVoice,
  shuffle,
  finish,
  noFinish,
}

extension PuzzleStatusX on PuzzleStateEnum {
  bool get isInitial => this == PuzzleStateEnum.initial;

  bool get isLoading => this == PuzzleStateEnum.loading;

  bool get isBuild => this == PuzzleStateEnum.build;

  bool get isRebuild => this == PuzzleStateEnum.rebuild;

  bool get isRebuildWithVoice => this == PuzzleStateEnum.rebuildWithVoice;

  bool get isShuffle => this == PuzzleStateEnum.shuffle;

  bool get isFinish => this == PuzzleStateEnum.finish;

  bool get isNoFinish => this == PuzzleStateEnum.noFinish;
}

class PuzzleState extends Equatable {
  PuzzleState({
    this.status = PuzzleStateEnum.initial,
    int? sizeSelected,
    List<PieceImage>? piecesImage,
    Position? emptyPosition,
    List<Piece>? puzzle,
    int? numPieceMovedByVoice,
  })  : sizeSelected = sizeSelected ?? 1,
        piecesImage = piecesImage ?? List.empty(),
        emptyPosition = emptyPosition ?? Position(0, 0),
        puzzle = puzzle ?? List.empty(),
        numPieceMovedByVoice = numPieceMovedByVoice ?? -1;

  final int sizeSelected;
  final List<PieceImage> piecesImage;
  final Position emptyPosition;
  final List<Piece> puzzle;
  final int numPieceMovedByVoice;
  final PuzzleStateEnum status;

  @override
  List<Object?> get props => [
        status,
        sizeSelected,
        piecesImage,
        puzzle,
        numPieceMovedByVoice,
        status,
      ];

  PuzzleState copyWith({
    int? sizeSelected,
    List<PieceImage>? piecesImage,
    Position? emptyPosition,
    List<Piece>? puzzle,
    int? numPieceMovedByVoice,
    PuzzleStateEnum? status,
  }) {
    return PuzzleState(
      sizeSelected: sizeSelected ?? this.sizeSelected,
      piecesImage: piecesImage ?? this.piecesImage,
      emptyPosition: emptyPosition ?? this.emptyPosition,
      puzzle: puzzle ?? this.puzzle,
      numPieceMovedByVoice: numPieceMovedByVoice ?? this.numPieceMovedByVoice,
      status: status ?? this.status,
    );
  }
}
