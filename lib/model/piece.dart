import 'package:puzzle/model/piece_image.dart';
import 'package:puzzle/model/position.dart';

class Piece {
  final PieceImage child;
  final bool isEmpty;
  final Position currentPosition;
  final int sizePuzzle;
  final Position? emptyPosition;

  Piece({
    required this.child,
    required this.isEmpty,
    required this.currentPosition,
    required this.sizePuzzle,
    this.emptyPosition,
  });
}
