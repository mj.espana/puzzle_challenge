import 'package:flutter/material.dart';

class PieceImage {
  final Widget piece;
  final int number;

  PieceImage(this.piece, this.number);
}
