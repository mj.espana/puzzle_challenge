import 'dart:async';

import 'package:flutter/material.dart';

class ChronometerWidget extends StatefulWidget {
  const ChronometerWidget({
    Key? key,
    this.isStop,
    this.isReset,
  }) : super(key: key);

  final bool? isStop;
  final bool? isReset;

  @override
  _ChronometerWidgetState createState() => _ChronometerWidgetState();
}

class _ChronometerWidgetState extends State<ChronometerWidget> {
  late Stopwatch _stopwatch;
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _stopwatch = Stopwatch();
    _timer = Timer.periodic(const Duration(milliseconds: 30), (timer) {
      setState(() {});
    });
    handleStartStop();
  }

  @override
  void didUpdateWidget(ChronometerWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.isStop!) {
      _stopwatch.stop();
    } else if (widget.isReset!) {
      _stopwatch.reset();
    }
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void handleStartStop() {
    if (_stopwatch.isRunning) {
      _stopwatch.stop();
    } else {
      _stopwatch.start();
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Icon(
          Icons.timer_outlined,
          color: Colors.white,
        ),
        const SizedBox(width: 5),
        Text(
          _formatTime(_stopwatch.elapsedMilliseconds),
          style: const TextStyle(
            fontSize: 28,
            color: Colors.white,
          ),
        ),
      ],
    );
  }

  String _formatTime(int milliseconds) {
    var secs = milliseconds ~/ 1000;
    var hours = (secs ~/ 3600).toString().padLeft(2, '0');
    var minutes = ((secs % 3600) ~/ 60).toString().padLeft(2, '0');
    var seconds = (secs % 60).toString().padLeft(2, '0');
    return "$hours:$minutes:$seconds";
  }
}
