import 'package:flutter/material.dart';
import 'package:puzzle/model/piece_image.dart';
import 'package:puzzle/model/position.dart';
import 'package:puzzle/utils/utils.dart';

class PieceWidget extends StatelessWidget {
  final PieceImage child;
  final bool isEmpty;
  final Position currentPosition;
  final int sizePuzzle;
  final Position? emptyPosition;
  final Function? onTap;

  const PieceWidget({
    Key? key,
    required this.child,
    required this.isEmpty,
    required this.currentPosition,
    required this.sizePuzzle,
    this.emptyPosition,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isEmpty) {
      return Padding(
        padding: const EdgeInsets.all(1),
        child: Container(
          width: 117,
          height: 117,
          padding: const EdgeInsets.all(8),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
          ),
        ),
      );
    } else {
      bool canMove = isDraggable(
          currentPosition.x, currentPosition.y, emptyPosition!, sizePuzzle);

      return GestureDetector(
        onTap: () => canMove ? onTap!(currentPosition) : {},
        child: Padding(
          padding: const EdgeInsets.all(1),
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(10),
                  ),
                  image: DecorationImage(
                    image: (child.piece as Image).image,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Positioned(
                top: 1,
                right: 5,
                child: Text(child.number.toString()),
              ),
            ],
          ),
        ),
      );
    }
  }
}
