import 'package:flutter/material.dart';

class PocWidget extends StatefulWidget {
  final double? width;
  final double? height;
  final double? padding;

  const PocWidget({
    Key? key,
    this.width = 100,
    this.height = 100,
    this.padding = 2,
  }) : super(key: key);

  @override
  State<PocWidget> createState() => _PocWidgetState();
}

class _PocWidgetState extends State<PocWidget> {
  double _valueX = 0.0;
  double _valueY = 0.0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _buildPiece(),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
              onPressed: () {
                setState(() {
                  _valueX = _valueX - widget.width! - widget.padding!;
                });
              },
              child: const Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(Colors.lightBlueAccent),
              ),
            ),
            const SizedBox(width: 10),
            TextButton(
              onPressed: () {
                setState(() {
                  _valueX = _valueX + widget.width! + widget.padding!;
                });
              },
              child: const Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ),
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(Colors.lightBlueAccent),
              ),
            ),
          ],
        ),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
              onPressed: () {
                setState(() {
                  _valueY = _valueY - widget.height! - widget.padding!;
                });
              },
              child: const Icon(
                Icons.arrow_upward,
                color: Colors.white,
              ),
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(Colors.lightBlueAccent),
              ),
            ),
            const SizedBox(width: 10),
            TextButton(
              onPressed: () {
                setState(() {
                  _valueY = _valueY + widget.height! + widget.padding!;
                });
              },
              child: const Icon(
                Icons.arrow_downward,
                color: Colors.white,
              ),
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(Colors.lightBlueAccent),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildPiece() {
    return TweenAnimationBuilder<double>(
      duration: const Duration(milliseconds: 500),
      tween: Tween(begin: 0, end: 1),
      builder: (context, value, child) {
        return Transform.translate(
          offset: Offset(_valueX, _valueY),
          child: child,
        );
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: widget.padding!),
        child: Container(
          width: widget.width,
          height: widget.height,
          decoration: const BoxDecoration(
            color: Colors.amber,
            borderRadius: BorderRadius.all(Radius.circular(8)),
          ),
        ),
      ),
    );
  }
}
