import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:puzzle/bloc/home/home_bloc.dart';
import 'package:puzzle/pages/components/text_button_widget.dart';
import 'package:puzzle/pages/puzzle/puzzle_page.dart';
import 'package:puzzle/utils/constants.dart';
import 'package:puzzle/utils/utils.dart';

class HomeWidget extends StatelessWidget {
  const HomeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeBloc, HomeState>(
      listener: (context, state) {
        if (state.status.isSuccess) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => PuzzlePage(
                piecesImage: state.puzzleImages,
                sizeSelected: state.optionSizeSelected,
                emptyPosition: state.emptyPosition,
              ),
            ),
          );
        }
      },
      builder: (context, state) {
        return Scaffold(
          body: Center(
            child: SingleChildScrollView(
              child: SizedBox(
                width: 600,
                height: 600,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    const Text(
                      'Puzzle Challenge',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 44,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        _buildImageOption(
                            image: image1,
                            numOption: 1,
                            context: context,
                            state: state),
                        _buildImageOption(
                            image: image2,
                            numOption: 2,
                            context: context,
                            state: state),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        _buildSizeOption(
                            size: size3, context: context, state: state),
                        _buildSizeOption(
                            size: size4, context: context, state: state),
                        _buildSizeOption(
                            size: size5, context: context, state: state),
                      ],
                    ),
                    TextButtonWidget(
                      text: state.status.isLoading ? 'Loading...' : 'Start',
                      color: Theme.of(context).colorScheme.secondary,
                      onPressed: () async {
                        if (state.status.isInitial) {
                          if (state.optionImageSelected == 0 ||
                              state.optionSizeSelected == 0) {
                            showMyDialog(
                              context,
                              _getDialog(context),
                            );
                          } else {
                            context.read<HomeBloc>().add(StartPuzzleEvent());
                          }
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildImageOption({
    required String image,
    required int numOption,
    required BuildContext context,
    required HomeState state,
  }) {
    return GestureDetector(
      onTap: () => context
          .read<HomeBloc>()
          .add(SelectImageOptionEvent(value: numOption)),
      child: Container(
        height: getSizeOptionImage(context),
        width: getSizeOptionImage(context),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(20)),
          border: Border.all(
            color: state.optionImageSelected == numOption
                ? Theme.of(context).colorScheme.secondary
                : Theme.of(context).colorScheme.secondary.withOpacity(0.5),
            width: getSizeBorderOption(context),
          ),
          color: Colors.white,
          image: DecorationImage(
            image: AssetImage(
              image,
            ),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget _buildSizeOption({
    required int size,
    required BuildContext context,
    required HomeState state,
  }) {
    return GestureDetector(
      onTap: () =>
          context.read<HomeBloc>().add(SelectSizeOptionEvent(value: size)),
      child: Container(
        height: getSizeOptionSize(context),
        width: getSizeOptionSize(context) * 2,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(15)),
          border: Border.all(
            color: state.optionSizeSelected == size
                ? Theme.of(context).colorScheme.secondary
                : Theme.of(context).colorScheme.secondary.withOpacity(0.5),
            width: getSizeBorderOption(context),
          ),
          color: Colors.white,
        ),
        child: Center(
          child: Text(
            '$size x $size',
            style: TextStyle(
              color: Theme.of(context).colorScheme.primary,
              fontSize: getSizeOptionText(context),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }

  Widget _getDialog(BuildContext context) {
    return AlertDialog(
      title: Text(
        'Ups!',
        style: TextStyle(
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
      content: const Text('Please, you have to select both options.'),
      actions: <Widget>[
        TextButton(
          child: Text(
            'Close',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
      actionsAlignment: MainAxisAlignment.center,
    );
  }
}
