import 'package:flutter/material.dart';
import 'package:puzzle/model/piece_image.dart';
import 'package:puzzle/model/position.dart';
import 'package:puzzle/pages/components/piece_widget.dart';
import 'package:puzzle/utils/utils.dart';

class Puzzle extends StatefulWidget {
  final int sizeSelected;
  final List<PieceImage> piecesImage;
  final Position emptyPosition;

  const Puzzle({
    Key? key,
    required this.sizeSelected,
    required this.piecesImage,
    required this.emptyPosition,
  }) : super(key: key);

  @override
  State<Puzzle> createState() => _PuzzleState();
}

class _PuzzleState extends State<Puzzle> {
  List<PieceWidget> puzzle = [];
  late Position positionEmptyPiece;

  @override
  void initState() {
    super.initState();
    positionEmptyPiece = widget.emptyPosition;
    puzzle = _buildPuzzle();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: getSizePuzzle(context),
          height: getSizePuzzle(context),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
          ),
          child: Center(
            child: Container(
              width: getSizeInsidePuzzle(context),
              height: getSizeInsidePuzzle(context),
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primary,
              ),
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: 1,
                  crossAxisCount: widget.sizeSelected,
                  mainAxisSpacing: 1,
                ),
                itemCount: puzzle.length,
                itemBuilder: (context, index) => puzzle[index],
                padding: const EdgeInsets.all(1),
                primary: false,
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<PieceWidget> _buildPuzzle() {
    List<PieceWidget> allPieces = [];
    List.generate(
      widget.sizeSelected,
      (i) => List.generate(
        widget.sizeSelected,
        (j) => allPieces.add(
            _getPiece(i, j, widget.piecesImage[i * widget.sizeSelected + j])),
      ),
    );
    return allPieces;
  }

  PieceWidget _getPiece(int i, int j, PieceImage piece) {
    return PieceWidget(
      child: piece,
      isEmpty: isEmptyPositionPiece(i, j, positionEmptyPiece),
      currentPosition: Position(i, j),
      emptyPosition: positionEmptyPiece,
      sizePuzzle: widget.sizeSelected,
      onTap: (position) {
        setState(() {
          puzzle[positionEmptyPiece.x * widget.sizeSelected +
                  positionEmptyPiece.y] =
              puzzle[position.x * widget.sizeSelected + position.y];
          positionEmptyPiece = position;
          puzzle = _rebuildPuzzle();
        });
      },
    );
  }

  List<PieceWidget> _rebuildPuzzle() {
    List<PieceWidget> allPieces = [];
    List.generate(
      widget.sizeSelected,
      (i) => List.generate(
        widget.sizeSelected,
        (j) => allPieces
            .add(_rebuildGetPiece(i, j, puzzle[i * widget.sizeSelected + j])),
      ),
    );
    return allPieces;
  }

  PieceWidget _rebuildGetPiece(int i, int j, PieceWidget pieceWidget) {
    return PieceWidget(
      child: pieceWidget.child,
      isEmpty: isEmptyPositionPiece(i, j, positionEmptyPiece),
      currentPosition: Position(i, j),
      emptyPosition: positionEmptyPiece,
      sizePuzzle: widget.sizeSelected,
      onTap: pieceWidget.onTap,
    );
  }
}
