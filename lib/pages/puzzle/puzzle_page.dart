import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:puzzle/bloc/puzzle/puzzle_bloc.dart';
import 'package:puzzle/model/piece_image.dart';
import 'package:puzzle/model/position.dart';
import 'package:puzzle/pages/puzzle/puzzle_widget.dart';

class PuzzlePage extends StatelessWidget {
  final int sizeSelected;
  final List<PieceImage> piecesImage;
  final Position emptyPosition;

  const PuzzlePage({
    Key? key,
    required this.sizeSelected,
    required this.piecesImage,
    required this.emptyPosition,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PuzzleBloc>(
      create: (context) => PuzzleBloc()
        ..add(LoadDataEvent(
          sizeSelected: sizeSelected,
          piecesImage: piecesImage,
          emptyPosition: emptyPosition,
        )),
      child: PuzzleWidget(
        sizeSelected: sizeSelected,
        piecesImage: piecesImage,
        emptyPosition: emptyPosition,
      ),
    );
  }
}
