import 'package:confetti/confetti.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:puzzle/bloc/puzzle/puzzle_bloc.dart';
import 'package:puzzle/model/piece_image.dart';
import 'package:puzzle/model/position.dart';
import 'package:puzzle/pages/chronometer/chronometer_widget.dart';
import 'package:puzzle/pages/components/piece_widget.dart';
import 'package:puzzle/pages/components/rounded_button_widget.dart';
import 'package:puzzle/pages/components/text_button_widget.dart';
import 'package:puzzle/utils/responsive.dart';
import 'package:puzzle/utils/utils.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';

class PuzzleWidget extends StatefulWidget {
  const PuzzleWidget({
    Key? key,
    required this.sizeSelected,
    required this.piecesImage,
    required this.emptyPosition,
  }) : super(key: key);

  final int sizeSelected;
  final List<PieceImage> piecesImage;
  final Position emptyPosition;

  @override
  State<PuzzleWidget> createState() => _PuzzleWidgetState();
}

class _PuzzleWidgetState extends State<PuzzleWidget> {
  final SpeechToText _speechToText = SpeechToText();
  late ConfettiController _confettiController;
  String _lastWords = '';
  bool enableMic = true;
  bool stopChrono = false;
  bool resetChrono = false;
  int numPieceMovedByVoice = -1;

  @override
  void initState() {
    super.initState();
    _initSpeech();
    _confettiController =
        ConfettiController(duration: const Duration(seconds: 5));
  }

  @override
  void dispose() {
    _confettiController.dispose();
    super.dispose();
  }

  void _initSpeech() async {
    await _speechToText.initialize();
    setState(() {});
  }

  void _startListening() async {
    enableMic = false;
    await _speechToText.listen(onResult: _onSpeechResult, localeId: 'es_ES');
    setState(() {});
  }

  void _stopListening() async {
    enableMic = true;
    await _speechToText.stop();
    context.read<PuzzleBloc>().add(MovePieceVoiceEvent(word: _lastWords));
    setState(() {});
  }

  void _onSpeechResult(SpeechRecognitionResult result) {
    setState(() {
      _lastWords = result.recognizedWords;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Responsive.isMobile(context)
            ? SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(height: 20),
                    _buildChronometerWidget(),
                    const SizedBox(height: 10),
                    _buildPuzzle(),
                    const SizedBox(height: 20),
                    _buildShuffleButton(context),
                    const SizedBox(height: 20),
                    _buildSolutionButton(context),
                    const SizedBox(height: 20),
                    _buildFinishButton(context),
                    const SizedBox(height: 20),
                    _buildMicButton(),
                    const SizedBox(height: 20),
                  ],
                ),
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _buildChronometerWidget(),
                      const SizedBox(height: 20),
                      _buildShuffleButton(context),
                      const SizedBox(height: 20),
                      _buildSolutionButton(context),
                      const SizedBox(height: 20),
                      _buildFinishButton(context),
                      const SizedBox(height: 20),
                      _buildMicButton(),
                      const SizedBox(height: 10),
                      _buildNumberVoiceText(),
                    ],
                  ),
                  const SizedBox(width: 20),
                  _buildPuzzle(),
                ],
              ),
      ),
    );
  }

  BlocConsumer<PuzzleBloc, PuzzleState> _buildPuzzle() {
    return BlocConsumer<PuzzleBloc, PuzzleState>(
      listener: (context, state) {
        if (state.status.isNoFinish) {
          setState(() {
            numPieceMovedByVoice = -1;
            resetChrono = false;
          });
          _confettiController.play();
          showMyDialog(
            context,
            _getUnFinishDialog(context),
          );
        } else if (state.status.isFinish) {
          setState(() {
            stopChrono = true;
            numPieceMovedByVoice = -1;
          });
          _confettiController.play();
          showMyDialog(
            context,
            _getFinishDialog(context),
          );
        } else if (state.status.isShuffle) {
          setState(() {
            resetChrono = true;
            numPieceMovedByVoice = -1;
          });
        } else if (state.status.isRebuildWithVoice) {
          setState(() {
            numPieceMovedByVoice = state.numPieceMovedByVoice;
            resetChrono = false;
          });
        } else {
          setState(() {
            numPieceMovedByVoice = -1;
            resetChrono = false;
          });
        }
      },
      builder: (context, state) {
        return !state.status.isInitial && !state.status.isLoading
            ? Container(
                width: getSizePuzzle(context),
                height: getSizePuzzle(context),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: Center(
                  child: Container(
                    width: getSizeInsidePuzzle(context),
                    height: getSizeInsidePuzzle(context),
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                    child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisSpacing: 1,
                        crossAxisCount: state.sizeSelected,
                        mainAxisSpacing: 1,
                      ),
                      itemCount: state.puzzle.length,
                      itemBuilder: (context, index) => PieceWidget(
                        child: state.puzzle[index].child,
                        isEmpty: state.puzzle[index].isEmpty,
                        currentPosition: state.puzzle[index].currentPosition,
                        sizePuzzle: state.puzzle[index].sizePuzzle,
                        onTap: (position) => context
                            .read<PuzzleBloc>()
                            .add(MovePieceEvent(position: position)),
                        emptyPosition: state.emptyPosition,
                      ),
                      padding: const EdgeInsets.all(1),
                      primary: false,
                    ),
                  ),
                ),
              )
            : const CircularProgressIndicator();
      },
    );
  }

  Text _buildNumberVoiceText() {
    return Text(
      numPieceMovedByVoice != -1 ? numPieceMovedByVoice.toString() : '',
      style: const TextStyle(
        fontSize: 24,
        color: Colors.white,
      ),
    );
  }

  Widget _buildMicButton() {
    return Column(
      children: [
        RoundedButtonWidget(
          color: Colors.red.withOpacity(0.5),
          child: IconButton(
            onPressed:
                _speechToText.isNotListening ? _startListening : _stopListening,
            icon: Icon(
              enableMic ? Icons.mic : Icons.mic_off,
              color: Colors.white,
            ),
          ),
        ),
        const SizedBox(height: 10),
        const Text(
          'Tap and say the number that you want to move',
          style: TextStyle(
            color: Colors.white,
            fontSize: 12,
          ),
        ),
      ],
    );
  }

  TextButtonWidget _buildFinishButton(BuildContext context) {
    return TextButtonWidget(
      text: 'Finish',
      onPressed: () => context.read<PuzzleBloc>().add(OnFinishEvent()),
      color: Colors.blueAccent.withOpacity(0.5),
    );
  }

  TextButtonWidget _buildSolutionButton(BuildContext context) {
    return TextButtonWidget(
      text: 'Solution',
      onPressed: () => showMyDialog(
        context,
        _getSolutionDialog(context),
      ),
      color: Colors.pinkAccent.withOpacity(0.5),
    );
  }

  TextButtonWidget _buildShuffleButton(BuildContext context) {
    return TextButtonWidget(
      text: 'Shuffle',
      onPressed: () => showMyDialog(
        context,
        _getShuffleDialog(context),
      ),
      color: Colors.amber.withOpacity(0.5),
    );
  }

  ChronometerWidget _buildChronometerWidget() {
    return ChronometerWidget(
      isStop: stopChrono,
      isReset: resetChrono,
    );
  }

  Widget _getShuffleDialog(BuildContext context) {
    return AlertDialog(
      title: Text(
        'Wow!',
        style: TextStyle(
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
      content: const Text('Are you sure that you want to shuffle?!'),
      actions: <Widget>[
        TextButton(
          child: Text(
            'Cancel',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        TextButton(
          child: Text(
            'Confirm',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
            context.read<PuzzleBloc>().add(
                  ShuffleEvent(
                    sizeSelected: widget.sizeSelected,
                    piecesImage: widget.piecesImage,
                  ),
                );
          },
        ),
      ],
      actionsAlignment: MainAxisAlignment.center,
    );
  }

  Widget _getSolutionDialog(BuildContext context) {
    final int count = widget.sizeSelected * widget.sizeSelected;
    return AlertDialog(
      title: Text(
        'Solution',
        style: TextStyle(
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
      content: SizedBox(
        width: 200,
        height: 235,
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisSpacing: 1,
            crossAxisCount: widget.sizeSelected,
            mainAxisSpacing: 1,
          ),
          itemCount: count,
          itemBuilder: (context, index) => index + 1 == count
              ? const SizedBox(
                  width: 40,
                  height: 40,
                )
              : Container(
                  width: 40,
                  height: 40,
                  child: Center(
                    child: Text(
                      '${index + 1}',
                      style: const TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.primary,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(3),
                    ),
                  ),
                ),
          padding: const EdgeInsets.all(1),
          primary: false,
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'Close',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ],
      actionsAlignment: MainAxisAlignment.center,
    );
  }

  Widget _getUnFinishDialog(BuildContext context) {
    return AlertDialog(
      title: Text(
        'Ups!',
        style: TextStyle(
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
      content: const Text('Continue trying it...'),
      actions: <Widget>[
        TextButton(
          child: Text(
            'Close',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ],
      actionsAlignment: MainAxisAlignment.center,
    );
  }

  Widget _getFinishDialog(BuildContext context) {
    return AlertDialog(
      title: Text(
        'Amazing!',
        style: TextStyle(
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
      content: SizedBox(
        height: 20,
        child: Column(
          children: [
            const Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Congratulations!! You have reached to resolve the puzzle',
              ),
            ),
            ConfettiWidget(
              confettiController: _confettiController,
              blastDirectionality: BlastDirectionality.explosive,
              shouldLoop: true,
              colors: const [
                Colors.green,
                Colors.blue,
                Colors.pink,
                Colors.orange,
                Colors.purple
              ],
            ),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'Close',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          onPressed: () => Navigator.of(context).pushNamed('/'),
        ),
      ],
      actionsAlignment: MainAxisAlignment.center,
    );
  }
}
