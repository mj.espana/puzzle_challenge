import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image/image.dart' as imglib;
import 'package:puzzle/model/position.dart';
import 'package:puzzle/utils/constants.dart';

Future<List<Widget>> getImageSplit(
    int imageSelected, int size, Position emptyPosition) async {
  List<Image> imagesList =
      await _splitImage(_getPathImage(imageSelected), size);
  List<Widget> imagesListWithEmpty = [];
  imagesListWithEmpty.addAll(imagesList.sublist(0, imagesList.length - 1));
  final int index = emptyPosition.x * size + emptyPosition.y;
  imagesListWithEmpty.shuffle();
  imagesListWithEmpty.insert(index, Container());
  return imagesListWithEmpty;
}

String _getPathImage(int imageSelected) {
  if (imageSelected == 1) {
    return image1;
  } else if (imageSelected == 2) {
    return image2;
  } else {
    return '';
  }
}

Future<List<Image>> _splitImage(String pathImage, int numSplit) async {
  final byteData = await rootBundle.load(pathImage);

  // convert image to image from image package
  imglib.Image? image = imglib.decodeImage(byteData.buffer.asUint8List());

  int x = 0, y = 0;
  int width = (image!.width / numSplit).round();
  int height = (image.height / numSplit).round();

  // split image to parts
  List<imglib.Image> parts = [];
  for (int i = 0; i < numSplit; i++) {
    for (int j = 0; j < numSplit; j++) {
      parts.add(imglib.copyCrop(image, x, y, width, height));
      x += width;
    }
    x = 0;
    y += height;
  }

  List<Image> output = [];
  for (var img in parts) {
    output.add(Image.memory(Uint8List.fromList(imglib.encodeJpg(img))));
  }

  return output;
}
