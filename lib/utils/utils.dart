import 'dart:math';

import 'package:flutter/material.dart';
import 'package:puzzle/model/position.dart';
import 'package:puzzle/utils/responsive.dart';

double getSizePuzzle(BuildContext context) {
  if (Responsive.isDesktop(context)) {
    return 550;
  } else if (Responsive.isTablet(context)) {
    return 480;
  } else {
    return 400;
  }
}

double getSizeInsidePuzzle(BuildContext context) {
  if (Responsive.isDesktop(context)) {
    return 478;
  } else if (Responsive.isTablet(context)) {
    return 420;
  } else {
    return 350;
  }
}

double getSizeOptionImage(BuildContext context) {
  if (Responsive.isDesktop(context) || Responsive.isTablet(context)) {
    return 200;
  } else {
    return 150;
  }
}

double getSizeOptionSize(BuildContext context) {
  if (Responsive.isDesktop(context) || Responsive.isTablet(context)) {
    return 60;
  } else {
    return 50;
  }
}

double getSizeOptionText(BuildContext context) {
  if (Responsive.isDesktop(context) || Responsive.isTablet(context)) {
    return 20;
  } else {
    return 16;
  }
}

double getSizeBorderOption(BuildContext context) {
  if (Responsive.isDesktop(context)) {
    return 10;
  } else if (Responsive.isTablet(context)) {
    return 8;
  } else {
    return 5;
  }
}

bool isDraggable(final int x, final int y, final Position positionEmptyPiece,
    int sizePuzzle) {
  bool draggable = false;
  int maxPosition = sizePuzzle - 1;
  int top = positionEmptyPiece.x - 1 <= 0 ? 0 : positionEmptyPiece.x - 1;
  int right = positionEmptyPiece.y + 1 >= maxPosition
      ? maxPosition
      : positionEmptyPiece.y + 1;
  int bottom = positionEmptyPiece.x + 1 >= maxPosition
      ? maxPosition
      : positionEmptyPiece.x + 1;
  int left = positionEmptyPiece.y - 1 <= 0 ? 0 : positionEmptyPiece.y - 1;

  if ((y == left && x == positionEmptyPiece.x) ||
      (y == right && x == positionEmptyPiece.x)) {
    draggable = true;
  }
  if ((x == top && y == positionEmptyPiece.y) ||
      (x == bottom && y == positionEmptyPiece.y)) {
    draggable = true;
  }

  return draggable;
}

bool isEmptyPositionPiece(
    final int i, final int j, final Position positionEmptyPiece) {
  return i == positionEmptyPiece.x && j == positionEmptyPiece.y;
}

Position getPositionEmptyPiece(int size) {
  final random = Random();
  return Position(random.nextInt(size), random.nextInt(size));
}

Future<void> showMyDialog(BuildContext context, Widget child) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return child;
    },
  );
}

int getNumberPiece(String word, int size) {
  int number = -1;
  switch (size) {
    case 3:
      {
        if (_getDictionary3x3Strings().containsKey(word)) {
          final dictionary = _getDictionary3x3Strings();
          number = dictionary[word]!;
        } else if (_getDictionary3x3Numbers().containsKey(word)) {
          final dictionary = _getDictionary3x3Numbers();
          number = dictionary[word]!;
        }
      }
      break;
    case 4:
      {
        if (_getDictionary4x4Strings().containsKey(word)) {
          final dictionary = _getDictionary4x4Strings();
          number = dictionary[word]!;
        } else if (_getDictionary4x4Numbers().containsKey(word)) {
          final dictionary = _getDictionary4x4Numbers();
          number = dictionary[word]!;
        }
      }
      break;
    case 5:
      {
        if (_getDictionary5x5Strings().containsKey(word)) {
          final dictionary = _getDictionary5x5Strings();
          number = dictionary[word]!;
        } else if (_getDictionary5x5Numbers().containsKey(word)) {
          final dictionary = _getDictionary5x5Numbers();
          number = dictionary[word]!;
        }
      }
      break;
    default:
      {}
      break;
  }
  return number;
}

Map<String, int> _getDictionary3x3Numbers() {
  return {
    '1': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
  };
}

Map<String, int> _getDictionary3x3Strings() {
  return {
    'uno': 1,
    'dos': 2,
    'tres': 3,
    'cuatro': 4,
    'cinco': 5,
    'seis': 6,
    'siete': 7,
    'ocho': 8,
  };
}

Map<String, int> _getDictionary4x4Numbers() {
  return {
    '1': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    '10': 10,
    '11': 11,
    '12': 12,
    '13': 13,
    '14': 14,
    '15': 15,
  };
}

Map<String, int> _getDictionary4x4Strings() {
  return {
    'uno': 1,
    'dos': 2,
    'tres': 3,
    'cuatro': 4,
    'cinco': 5,
    'seis': 6,
    'siete': 7,
    'ocho': 8,
    'nueve': 9,
    'diez': 10,
    'once': 11,
    'doce': 12,
    'trece': 13,
    'catorce': 14,
    'quince': 15,
  };
}

Map<String, int> _getDictionary5x5Numbers() {
  return {
    '1': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    '10': 10,
    '11': 11,
    '12': 12,
    '13': 13,
    '14': 14,
    '15': 15,
    '16': 16,
    '17': 17,
    '18': 18,
    '19': 19,
    '20': 20,
    '21': 21,
    '22': 22,
    '23': 23,
    '24': 24,
  };
}

Map<String, int> _getDictionary5x5Strings() {
  return {
    'uno': 1,
    'dos': 2,
    'tres': 3,
    'cuatro': 4,
    'cinco': 5,
    'seis': 6,
    'siete': 7,
    'ocho': 8,
    'nueve': 9,
    'diez': 10,
    'once': 11,
    'doce': 12,
    'trece': 13,
    'catorce': 14,
    'quince': 15,
    'dieciseis': 16,
    'diecisiete': 17,
    'dieciocho': 18,
    'diecinueve': 19,
    'veinte': 20,
    'veintiuno': 21,
    'veintidos': 22,
    'veintitres': 23,
    'veinticuatro': 24,
  };
}
